# Development Plan for JavaScript Beginner

Please check [issues](https://gitlab.com/stalniy/idp/boards) for details

## Перед тим як почати

1. Треба [зареєструватись в gitlab](https://gitlab.com/users/sign_in)
2. Треба розібратись з базовими операціями в Git. [Git book](https://git-scm.com/book/uk/v2) в цьому допоможе.
   * git status
   * git add & git commit
   * git push & git pull
   * git merge 
   * git branch
   * Merge Requests

## Процес

1. На кожен пункт із плану де є тестова задачу, треба створити гілку. гулку називаємо `task_НОМЕР_ЗАДАЧІ`. 
   Якщо у вимогах є декілька задач, створюємо на кожну з них окрему гілку. Для прикладу візьмемо https://gitlab.com/stalniy/idp/issues/6 . Там є 5 тестових задач. 
   На кожну задачу має бути гілка - `task_6_1`, на наступну `task_6_2` і т.д.
2. Файли що відносяться до тієї задачі мають лежати в папці `tasks`. Наприклад, для #6 має бути папка `tasks/task_6`. В тій папці відповідно мають лежати файли, що відносяться до задачі. Файли можна називати як хочеш.
3. Коли задача завершена створюється Merge Request (MR) на master гілку.
3. Як тільки приходить MR, його перевіряють. Якщо все добре, ставлять 1 лайк, якщо треба щось виправити на конкретно строчку коду пишуть коментар
4. Якщо MR має лайк, його автор має замерджити в `master`. 
5. Якщо лайка нема, то треба виправити коментарі
6. Якщо коментар не ясно описаний, треба задати питання в новому коментарі до вказаної дискусії

## Ресурси для вивчення

* Git, система контролю версій https://git-scm.com/book/uk/v2
* багато корисного можна знайти на MDN (https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics)
* все коротко про JS http://bonsaiden.github.io/JavaScript-Garden/ru/
* все детально про JS http://learn.javascript.ru/ (вчи ES6 - це новий стандарт для JS)
* по структурам данних https://coursehunters.net/course/javascript-algoritmy-i-struktury-dannyh-master-klass
* курси по JS (і не тільки) https://coursehunters.net/frontend/javascript
* про HTML/CSS https://webref.ru/  (тут тоже треба прочитати про CSS препроцесори, наприклад SCSS)
* про PostCSS https://postcss.org/ (розібратись що воно таке і для чого)
* про Bash базу можна вивчити тут - http://freaksidea.com/linux/page-2/ (починай з http://freaksidea.com/linux/show-26-chto-nuzhno-znat-novichku-v-linux)


## Читати/розібратись після закінчення плану
* про Security https://www.hacksplaining.com/lessons
* https://vuejs.org/
* https://vuex.vuejs.org/
* "Clean Code" Robert Martin. https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882
* "Refactoring. Improving code of existing apps" Martin Fowler
* https://github.com/MostlyAdequate/mostly-adequate-guide
* https://eslint.org/
* https://github.com/airbnb/javascript
* https://webpack.js.org/
* https://reactjs.org/
* https://redux.js.org/
* https://graphql.org/learn/
* https://redis.io/