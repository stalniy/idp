// 1 variant
function sum() {
  let result = 0;
  for (let i = 0; i < arguments.length; i++) {
    result += arguments[i];
  }
  return result;
}

// 2 variant
function sum(...rest) {
  let result = 0;
  for (let i = 0; i < rest.length; i++) {
    result += rest[i];
  }
  return result;
}
