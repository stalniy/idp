// 1 variant
const arr = [1, 3, 7, 1, 3, 9, 8, 7];
function removeDuplicates(arr) {
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    if (!result.includes(arr[i])) {
      result.push(arr[i])
    }
  }
  return result;
 }
console.log(removeDuplicates(arr));


// 2 variant
const arr = [1, 3, 7, 1, 3, 9, 8, 7];
function removeDuplicates(arr) {
  let result = Array.from(new Set(arr));
  return result;
}
console.log(removeDuplicates(arr));
