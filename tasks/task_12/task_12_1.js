
function clock () {
  let Data = new Date();
  let Hours = Data.getHours();
  let Minutes = Data.getMinutes();
  let Seconds = Data.getSeconds();
  if (Hours < 10) {
    Hours = "0" + Hours
  }
  if (Minutes < 10) {
    Minutes = "0" + Minutes
  }
  if (Seconds < 10) {
    Seconds = "0" + Seconds
  }
document.getElementById('clock').innerHTML = Hours + ":" + Minutes + ":" + Seconds;
}
setInterval(clock, 1000);
window.onload = clock;
