
const a = 1;
const b = 2;
const c = 1;
const Discriminant = Math.pow(b,2) - 4 * a * c;

if (Discriminant < 0 || a == 0) {
  alert("No solution.");
} else if (Discriminant > 0) {
  const x1 = (- b + Math.sqrt(Discriminant)) / 2 * a ;
  const x2 = (- b - Math.sqrt(Discriminant)) / 2 * a;

  alert("We have two solutions:\n\r" +
    " - first solution:" + " " + x1 + "\n\r" +
    " - second solution:" + " " + x2 );
} else if (Discriminant == 0) { 
  const x = -b / 2 * a;
  alert("We have one solution:" + " " + x ) ;
}
