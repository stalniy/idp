// 1 variant

const numbers = [1, 2, 7, 8, 299, 4, 1, 0, 100];
let maxNum = numbers[0];

for (let i = 1; i < numbers.length; i++) {
  if (numbers[i] > maxNum) {
    maxNum = numbers[i];
  }
}

alert(maxNum);


// 2 variant

const numbers = [1, 2, 7, 8, 4, 1, 0, 100, 299];
alert( Math.max.apply (null, numbers) );



// 3 variant

const numbers = [1, 2, 7, 8, 4, 1, 0, 100, 299];
alert( Math.max.apply (Math, numbers) );
