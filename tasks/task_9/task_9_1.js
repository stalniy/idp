class Point{
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
  plus(second) {
    return new Point(this.x + second.x, this.y + second.y)
  }
}
console.log(new Point(1, 2).plus(new Point(2, 1)));
