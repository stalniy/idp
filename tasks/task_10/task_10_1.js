const user = {}

function log() {
  console.log(this === user)
}

// this === user
// 1, 2 - параметри

// 1 variant
log.call(user, 1, 2);

// 2 variant
log.apply(user, [1, 2]);

// 3 variant
let ThirdVariant = log.bind(user, 1);
ThirdVariant();
