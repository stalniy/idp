


// 1 variant


const numbers = [1, 2, 3];
let sum = 0;

for( let i = 0; i < numbers.length; i++) {
  sum += numbers[i]
} console.log(sum);





// 2 variant

const numbers = [1, 2, 3];
const result = numbers.reduce(function(sum, current) {
  return sum + current;
}, 0);

console.log( result );
