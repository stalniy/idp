
// 1 variant

const string = 'hello';
console.log(string.split('').reverse().join(''));


// 2 variant

const string = 'hello';
let newStr = '';
for (let i = string.length - 1; i >= 0; i--) {
newStr += string[i];
}
console.log(newStr);
