
const string = 'name,-age';
const arrString = string.split(/\s*,\s*/);
const newArray = [];

for (let i = 0; i < arrString.length; i++) {
  if (arrString[i].startsWith("-",0)) {
    newArray[i] = {
      field: arrString[i].substr(1),
      order: -1
    }
  } else {
    newArray[i] = {
      field: arrString[i],
      order: 1
    }
  }
}

console.log(newArray);
